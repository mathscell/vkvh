import os
import wx
import wx.grid as gridlib
from pathlib import Path
import wx.lib.agw.customtreectrl as CT
import wx.lib.gizmos as gizmos  
import wx.adv
from wx.lib.wordwrap import wordwrap
import kvh
import wx.lib.scrolledpanel as scrolled
from parcours import *
import  time
import wx.lib.dialogs
import wx.aui as aui
RELATIVEWIDTHS = False
####################################################
wildcard="All files (*.*)|*.*"

class PanelOne(aui.AuiNotebook):
	def __init__(self, parent):
		aui.AuiNotebook.__init__(self, parent,-1,size=(10,10))
		my_sizer = wx.BoxSizer(wx.VERTICAL)
		self.SetMinSize(wx.Size(1000,1000))
	

########################################################################
class MyTree(wx.TreeCtrl):
	def __init__(self,parent,id,pos,size,style):
		wx.TreeCtrl.__init__(self,parent,id,pos,size,style)

########################################################################



########################################################################
########################################################################
class HelpFrame(wx.MiniFrame):
	def __init__(
		self,parent, title, pos=wx.DefaultPosition,size=wx.DefaultSize,style=wx.DEFAULT_FRAME_STYLE):
			wx.MiniFrame.__init__(self,parent,-1,title,pos,size,style)
			panel=wx.Panel(self,-1)
			self.Bind(wx.EVT_CLOSE,self.OnCloseWindow)
			self.sizer=wx.BoxSizer(wx.VERTICAL)
			st=wx.StaticText(self,label="How to use VKVH: \n File -> Open -> Select a file kvh")
			
	def OnCloseWindow(self,event):
		self.Destroy()
########################################################################
class MyForm(wx.Frame):
 
	#----------------------------------------------------------------------
	def __init__(self):
		
		
		wx.Frame.__init__(self, None, wx.ID_ANY, 
						  "VKVH")
		self.SetMinSize(wx.Size(1000,1000))
		self.panel_one = PanelOne(self)
		
		self.Bind(wx.EVT_SIZE, self.OnSize)
		
		
		self.sizer = wx.BoxSizer(wx.VERTICAL)
		self.sizer.Add(self.panel_one, 1, wx.EXPAND)
	
		self.SetSizer(self.sizer)
		
		
		menubar = wx.MenuBar()
		fileMenu = wx.Menu()
		helpMenu=wx.Menu()
		
		open_item = fileMenu.Append(wx.ID_ANY, 
												  "Open", 
												  "")
		close_menu_item=fileMenu.Append(wx.ID_ANY,"Quit")
		
		
		aide_menu_item=helpMenu.Append(wx.ID_ANY,"Help")
		infos_menu_item=helpMenu.Append(wx.ID_ANY,"Information")
		
		
		self.Bind(wx.EVT_MENU,self.onSwitchAide, aide_menu_item)
		
		self.Bind(wx.EVT_MENU, self.OnButton, open_item)
		  
		self.Bind(wx.EVT_MENU,self.onSwitchInfo,infos_menu_item)
		self.Bind(wx.EVT_MENU,self.onQuit,close_menu_item)
		
		menubar.Append(fileMenu, '&File')
		menubar.Append(helpMenu,'&Help')
		self.SetMenuBar(menubar)
		
		
		self.InitUI()
		self.statusbar=self.CreateStatusBar(1)
		self.statusbar.SetStatusText("...")
		self.Bind

		

	#----------------------------------------------------------------------
	def OnButton(self,evt):
		
		dlg=wx.FileDialog(
		self, message="Choose a file",
		defaultDir=os.getcwd(),
		defaultFile="",
		wildcard=wildcard,
		style=wx.FD_OPEN | wx.FD_MULTIPLE | wx.FD_CHANGE_DIR|wx.FD_FILE_MUST_EXIST|wx.FD_PREVIEW)
		
		if dlg.ShowModal()==wx.ID_OK:
			path=dlg.GetPaths()
			self.panel_one.win=wx.Panel(self.panel_one,id=1,size=(1000,1000))
			
			self.toolbar.EnableTool(111,True)
			self.toolbar.EnableTool(222,True)
			self.toolbar.EnableTool(333,True)
			self.toolbar.EnableTool(444,True)
			self.toolbar.EnableTool(555,True)

			
			self.tree=TreeCreation(self.panel_one.win,path)
			
			self.tree.GetMainWindow().Bind(wx.EVT_RIGHT_UP, self.OnRightUp)
			self.tree.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.OnActivate)
			
			
			
			text=os.path.basename(path[0])
			
			self.panel_one.AddPage(self.panel_one.win,text)
			
			dlg.Destroy()


################# SIZER ###########################
			self.panel_one.win.sizer=wx.BoxSizer(wx.VERTICAL)
			self.panel_one.win.sizer.Add(self.tree,0,flag=wx.ALIGN_LEFT,border=100)
		
		
			
			"""self.fbtn=wx.Button(self.panel_one.win,0,"Find")
			self.btn=wx.Button(self.panel_one.win,1,"Collapse all")
			self.btn2=wx.Button(self.panel_one.win,2,"Close sub-elements")
			
			self.panel_one.sizer=wx.BoxSizer(wx.HORIZONTAL)
			self.panel_one.sizer.Add(self.fbtn,1,wx.ALIGN_TOP,0)
			self.panel_one.sizer.Add(self.btn,1,wx.ALIGN_TOP,0)
			self.panel_one.sizer.Add(self.btn2,1,wx.ALIGN_TOP,0)
		
			self.panel_one.SetSizer(self.panel_one.sizer)
			
			#self.panel_one.sizer.Fit(self)
			self.panel_one.Layout()

			self.Bind(wx.EVT_BUTTON,self.on_button, self.fbtn)
			self.Bind(wx.EVT_BUTTON,self.on_collapse, self.btn)
			self.Bind(wx.EVT_BUTTON,self.on_sub_close, self.btn2)
			self.Bind(wx.EVT_FIND, self.on_find)
			self.Bind(wx.EVT_FIND_NEXT,self.on_find)
			self.Bind(wx.EVT_FIND_CLOSE, self.OnFindClose)"""
			
			self.pos=0
			self.size=0
			
			
			
			self.panel_one.win.SetSizer(self.panel_one.win.sizer)
			self.panel_one.win.sizer.Fit(self)
			self.panel_one.win.Layout()
############################################################
	
	def InitUI(self):
		self.toolbar=self.CreateToolBar()
		
		fbtn=self.toolbar.AddTool(111,bitmap=wx.Bitmap('images/search.png'),bmpDisabled=wx.NullBitmap,label="Find",shortHelp="Find an element", longHelp="Find an element in the file", kind= wx.ITEM_NORMAL)
		vtool=self.toolbar.AddTool(222,bitmap=wx.Bitmap('images/value.jpg'),bmpDisabled=wx.NullBitmap,label="VALUE",shortHelp="Search Value", longHelp=" Search Value", kind=wx.ITEM_CHECK)
		rtool=self.toolbar.AddTool(333,bitmap=wx.Bitmap('images/key.png'),bmpDisabled=wx.NullBitmap,label="KEY",shortHelp="Search Key", longHelp=" Search Key", kind=wx.ITEM_CHECK)
		sep=self.toolbar.AddSeparator()
		
		collapsebtn=self.toolbar.AddTool(444,bitmap=wx.Bitmap('images/collapse.png'),bmpDisabled=wx.NullBitmap,label="Collapse",shortHelp="Collapse all", longHelp="Collapse all elements in the file", kind= wx.ITEM_NORMAL)
		subcollapsebtn=self.toolbar.AddTool(555,bitmap=wx.Bitmap('images/subcollapse.png'),bmpDisabled=wx.NullBitmap,label="Hide",shortHelp="Hide sub-elements", longHelp="Hide sub-elements", kind= wx.ITEM_NORMAL)
		
		self.toolbar.EnableTool(111,False)
		self.toolbar.EnableTool(222,False)
		self.toolbar.EnableTool(333,False)
		self.toolbar.EnableTool(444,False)
		self.toolbar.EnableTool(555,False)
		
		
		
		
		self.toolbar.Realize()
		self.toolbar.Bind(wx.EVT_TOOL_RANGE,self.OnClick)
		
		
	def OnClick(self,event):
		print(event.GetId())
		
		
		if self.toolbar.GetToolState(222)==True:
			value_param=1
		else:
			value_param=0
		if self.toolbar.GetToolState(333)==True:
			key_param=1
		else:
			key_param=0
		
		if event.GetId()==111 and key_param+value_param!=0:
			self.on_button(wx.EVT_FIND)
			self.Bind(wx.EVT_FIND, self.on_find)
			self.Bind(wx.EVT_FIND_NEXT,self.on_find)
			self.Bind(wx.EVT_FIND_CLOSE, self.OnFindClose)
		
		elif event.GetId()==111 and key_param+value_param==0:
			dlg=wx.MessageDialog(self,'Please, select search parameters.','Error',wx.OK|wx.ICON_INFORMATION)
			dlg.ShowModal()
			dlg.Destroy()
			print("NO PARAMS")
		
		
		
		if event.GetId()==444:
			self.on_collapse(wx.EVT_BUTTON)
		if event.GetId()==555:
			self.on_sub_close(wx.EVT_BUTTON)
		
		
	
		
		
	def on_collapse(self,event):
		self.tree.Collapse(self.tree.GetRootItem())
		
	def on_sub_close(self,event):
		item=self.tree.GetSelection()
		if self.tree.ItemHasChildren(item):
			i=1
			count_children=self.tree.GetChildrenCount(item,False)
			child,cookie=self.tree.GetFirstChild(item)
			while i<count_children:
				self.tree.Collapse(child)
				child,cookie=self.tree.GetNextChild(item,cookie)
				i+=1
		
		self.tree.Collapse(item)
		

			
			
			
			
	def on_button(self,event):
		self.txt=self.tree.GetName()
		self.fdata=wx.FindReplaceData()
		self.dlg=wx.FindReplaceDialog(self.tree,self.fdata,'Find')
		self.dlg.Show()
	
	def on_find(self,event):
		
		"""lst=['Key','Value']
		dlg_choice=wx.MultiChoiceDialog(self,"Select parameters for the search","Parameters Selection",lst)
		dlg_choice.ShowModal()
		selections=dlg_choice.GetSelections()
		strings=[lst[x] for x in selections]
		dlg_choice.Destroy()"""
		
		ev=event.GetEventCategory()

		evtype=event.GetEventType()

		if self.toolbar.GetToolState(222)==True:
			value_param=1
		else:
			value_param=0
		if self.toolbar.GetToolState(333)==True:
			key_param=1
		else:
			key_param=0

		fstring=self.fdata.GetFindString()

		
		flags=self.fdata.GetFlags()
		
		
		
		
		self.pos=self.txt.find(fstring,self.pos)
		self.size=len(fstring)

		
		
		if evtype==10095:
			self.evtype=10095
			print("start root")
			root=self.tree.GetRootItem()
			#result=parcours_find_next(self,self.tree,root,fstring,key_param,value_param,flags)
			result=parcours_tree_by_label(self,self.tree,fstring,key_param,value_param,flags)
			self.tree.result=result
			if result is not None:
				print("MATCH=",self.tree.GetItemText(result))
				self.tree.SelectItem(result)
				root=self.tree.GetRootItem()
				statustext=PathCreation(self)
				stxt=""
				for elem in statustext:
					stxt+=str(elem)
					stxt+="/"
				self.statusbar.SetStatusText(stxt)
				parent=self.tree.GetItemParent(result)
				print("brunch expanded",self.tree.GetItemText(parent))
				self.tree.Expand(parent)
				"""if self.tree.ItemHasChildren(result):
					self.tree.Expand(result)
					print("children expanded",self.tree.GetItemText(result))"""
				#parent=self.tree.GetItemParent(parent)
				"""while parent != root:
					self.tree.Expand(parent)
					print("brunch expanded",self.tree.GetItemText(parent))
					parent=self.tree.GetItemParent(parent)"""
					
					
			else:
				dlg=wx.MessageDialog(self,'Element does not exist. Try another text','Error',wx.OK|wx.ICON_INFORMATION)
				dlg.ShowModal()
				dlg.Destroy()
				print("NO MATCH")
				
		if evtype==10096:
			print("INSIDE FIND NEXT")
			result=self.tree.result
			#print("Start search from ", self.tree.GetItemText(result))
			if result is None:
				dlg=wx.MessageDialog(self,'Element does not exist. Try another text','Error',wx.OK|wx.ICON_INFORMATION)
				dlg.ShowModal()
				dlg.Destroy()
				print("NO MATCH inside 10096")
			else:
				sibling=self.tree.GetNextSibling(result)
				if sibling is not None:
					print("try sibling: ",self.tree.GetItemText(sibling))
					parent=self.tree.GetItemParent(result)
					print("its parent: ", self.tree.GetItemText(parent) if parent is not None else "None")
					if parent is not None:
						result=parcours_find_next(self,self.tree,sibling,fstring,key_param,value_param,flags)
						
						
						
				else:

					parent=self.tree.GetItemParent(result)	
					sibling=self.tree.GetNextSibling(parent)
					print("try another sibling: ",self.tree.GetItemText(sibling))
					result=parcours_find_next(self,self.tree,sibling,fstring,key_param,value_param,flags)
				
				self.tree.result=result
				if result is not None:
					
					print("RESULT TEXT: ",self.tree.GetItemText(result))
					#print("Match DEF")
				else:
					print("No match DEF")
		
		
	

	
	
	
	def OnFindClose(self,evt):
		evt.GetDialog().Destroy()


	#----------------------------------------------------------------------

	#----------------------------------------------------------------------
	def onSwitchAide(self,event):
		with open("help.txt","r") as f:
			msg=f.read()
		dlg=wx.lib.dialogs.ScrolledMessageDialog(self,msg,"Help")
		dlg.ShowModal()
		

	#----------------------------------------------------------------------
	def onSwitchInfo(self,event):
		info=wx.adv.AboutDialogInfo()
		info.Name="Information"
		info.Version="version"
		info.Help="help"
		info.Copyright="(c) copyright"
		info.Description=wordwrap("description",350,wx.ClientDC(self))
		info.Developers=["Serguei Sokol","Maria Morozova"]
		
		wx.adv.AboutBox(info)
	
	#----------------------------------------------------------------------
	
	def onQuit(self,event):
		self.Close()
	#----------------------------------------------------------------------
	def OnActivate(self, evt):
		print("OnActivate")
		statustext=PathCreation(self)
		stxt=""
		for elem in statustext:
			stxt+=str(elem)
			stxt+="/"
		self.statusbar.SetStatusText(stxt)
		
		


	def OnRightUp(self, evt):
		pos = evt.GetPosition()
		item, flags, col = self.tree.HitTest(pos)
		if item:
			print("OnRightUp")
			
			
						  
	def OnSize(self, evt):
		#self.SetSize(self.panel_one.GetSize())
		self.panel_one.SetSize(self.GetSize())
		if "tree" in self.__dict__:
			self.tree.SetSize(self.panel_one.GetSize())

		
if __name__ == "__main__":
	app = wx.App(False)
	frame = MyForm()
	frame.Show()
	app.MainLoop()
