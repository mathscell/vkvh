Code documentation
==================

How it works
------------

``vkvh`` represents kvh-files in the form of a tree based on a widget of wxPython library ``wx.lib.agw.hypertreelist.HyperTreeList``. Each tree has a coupled flat list with key-value pairs ``tree.itlist``. This flat list is used as helper for searching and editing tree items.

Each tree is shown in a ``wx.Panel`` which, in turn, is a tab in the ``wx.aui.AuiNotebook`` shown in the main window.
	
Code files
----------
- ``vkvh.py`` - a file which contains the code for the *interface structure*. The *search* interface is also defined in this file. It uses the functions from the ``parcours.py`` to create trees and to realize search procedures.

- ``parcours.py`` - a file which contains functions used for *construction* and *manipulation* of a tree. 

Details of ``vkvh.py``
----------------------
The file ``vkvh.py`` contains mostly the code for GUI (graphical user interface) of the application.

.. automodule:: vkvh.vkvh
 :members:
 :undoc-members:

Details of ``parcours.py``
--------------------------

A tree is created by a method ``make_tree()`` of a class ``TreeCreation``. The latter creates a new panel to be added to the notebook. The main search helper is the function ``parcours_tree_by_label()``

.. automodule:: vkvh.parcours
 :members:
 :undoc-members:
