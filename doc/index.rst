.. VKVH documentation master file, created by
   sphinx-quickstart on Mon Jun 27 11:09:57 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VKVH's documentation!
================================

``vkvh`` is a wxPython viewer/editor for KVH (Key-Value Hierarchy) files.  It represents kvh-files in a *simple* and *clear* tree-like structure. It offers also some basic editing features:
   - edit key and value labels
   - add sub-key to already existing key
   - add new key at the same level as some current key, before or after it.

.. toctree::
   :maxdepth: 2
   
   usage
   code



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
