User's Manual
=============

.. _instalation:

Installation
------------

To use ``vkvh``, first install it using pip:

.. code-block:: console

   $ pip install vkvh
   

Open/Save/Close/Quit
--------------------

In the menu bar, click on *File > Open* (or press *Ctrl-O* as keyboard shortcut) to choose a file (or several ones) to open. They can also be opened from a command line:

.. code-block:: console

   $ vkvh file.kvh [file2.kvh [...]]

Each file is opened in its own tab, in a notebook shown in the main window.

An edition of a new empty file can be started with *File > New* or *Ctrl-N*.

To save an edited file, click on *File > Save* (or *Ctrl-S*). You can do *File > Save As ...* to save the current tab under a new name.

To close a file in ``vkvh``, click on the cross icon on the corresponding tab. If the file to close was edited and not saved yet, a confirmation will be asked.

To quit the application, do one of the following:

	- click on *File > Quit* (or *Ctrl-Q*)
	- close the ``vkvh`` window.

If one the open tabs was edited but not saved, a confirmation will be asked before quitting.

Expand/Collapse
---------------

The file content is represented in an hierarchical tree form organized in 3 columns:

	- *N°* indicating the flat number of the item in the content
	- *Key*
	- *Value*.

An item with sub-items can be expanded or collapsed by clicking on the triangle (or "+"/"-" sign depending on platform) on the left side of a key. To expand all sub-items of a key, right-click on it and select *Expand all in <key name>*. The same is true for collapsing all sub-items. Please note, that on big trees, these operations can take considerable time. To find a particular item, it is much more fast to use *Search* tool.

Search text in keys and values
------------------------------

In search field in the toolbar, enter some text and press Enter. By default, the text is searched in keys only, with partial word matching, in downward direction and in case insensitive manner. To search the text in values too or in values exclusively, or for whole words only etc. check or uncheck the corresponding options in *Options* menu. Options include:

	- *Search in Keys*
	- *Search in Values*
	- *Whole world* (match the whole words only)
	- *Backward* (make a search in backward direction)
	- *Match Case* (make the search case sensitive).
	
If the text is found, the corresponding Key/Value pair is selected and made visible. It is also shown in status bar. Otherwise, an error message is shown.
	
To search for the next occurrence, press Enter again.

During a session, the last 5 searched terms are saved in a search history which can be accessed by clicking on the *Search* icon in the search field (most often it takes the shape of a *magnifying glass*). The search history is lost when you restart ``vkvh``.

Edit Key/Value
--------------

To change the label of a key or value just double click on it. An in-place editing form appears letting to edit new content. Note that the *Root* key cannot be edited.

To add a new key, right-click on an existing key and choose from floating menu:

	- add a sub-key or
	- add a key at the same level before or after the current key.

Once the new key is created, double click on its value column to create corresponding value if needed. Note that no new sub-key can be created for a key having non empty *Value* field.

To delete an item and all its sub-items, right-click on a key and choose *Delete item <key name>*.

If a tree was edited, a star character "*" is shown before the file name in the tab title. When the tab is saved, the star is removed.

Help
----

Menu *Help > Manual* provides this page as well as some legal information in *Help > About*.
